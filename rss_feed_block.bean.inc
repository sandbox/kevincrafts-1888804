<?php
/**
 * @file
 * rss_feed_block.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function rss_feed_block_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'rss_feed';
  $bean_type->label = 'RSS Feed';
  $bean_type->options = '';
  $bean_type->description = 'A block with parses rss feeds.';
  $export['rss_feed'] = $bean_type;

  return $export;
}
