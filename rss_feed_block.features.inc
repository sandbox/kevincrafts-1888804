<?php
/**
 * @file
 * rss_feed_block.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rss_feed_block_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "bean_admin_ui" && $api == "bean") {
    return array("version" => "5");
  }
}
