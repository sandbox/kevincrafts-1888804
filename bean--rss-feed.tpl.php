<?php 
// See if snippets should display
if ($content['field_rss_block_snippets'][0]['#markup'] == "Yes") {
  $snippet = "rssoutput += '<div class=\"rss-feed-snippet\">' + entry.contentSnippet + '</div>'"; 
}
$feedID = rand(); 
?>


<div class="rss-feed-block" id="feed<?php print $feedID; ?>"></div>
<script type="text/javascript">
  function init(){ 
      loadFeed<?php print $feedID; ?>({ 
        url : '<?php print $content['field_rss_block_feed_url'][0]['#markup']; ?>', 
        divId : 'feed<?php print $feedID; ?>', 
        noOfFeed : <?php print $content['field_rss_block_feed_items'][0]['#markup']; ?> 
      }); 
  } 

  function loadFeed<?php print $feedID; ?>(opt_options){ 
    var feed = new google.feeds.Feed(opt_options.url); 
    var rssoutput="<ul class='rss-feed-items'>"
    feed.setNumEntries(opt_options.noOfFeed); 
    feed.load(function(result){ 
      if(!result.error){ 
        var feeddiv = jQuery('#' + opt_options.divId), 
          li = '<li>' + result.feed.link + '</li>', 
          divOne = ''; 
        jQuery('#vtab ul').append(li); 
        for(var i=0; i<result.feed.entries.length; i++){ 
          var entry = result.feed.entries[i]; 
          rssoutput += '<li><a href="' + entry.link + '" target="_blank">' + entry.title + '</a>';
          
          <?php print $snippet; ?>;
          
          rssoutput+='</li>'; 
        } 
        jQuery('#' + opt_options.divId).html(rssoutput); 
      } 
      rssoutput += '<ul>';
    }); 
    
  } 
  

  google.setOnLoadCallback(init); 
</script>
<?php
  print render($content['field_rss_block_link']);
?>